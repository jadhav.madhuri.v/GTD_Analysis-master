import folium
import plotly.tools as tls 
import plotly.express as px
import plotly.figure_factory as ff
from scipy.interpolate import interp1d
import plotly.graph_objects as go
import matplotlib.pyplot as plt 
import cufflinks as cf
import numpy as np 
import pandas as pd
import seaborn as sns
###############
# Basic Analysis and Visualization
import pandas as pd
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import math
from datetime import timedelta
# Mapping
import geopandas
import geopy
from geopy.geocoders import Nominatim
import folium
from geopy.extra.rate_limiter import RateLimiter
from folium import plugins
from folium.plugins import MarkerCluster
# Statistical OLS Regression Analysis
import statsmodels.api as sm
from statsmodels.compat import lzip
from statsmodels.formula.api import ols
#Scipy sklearn Predictions
from sklearn.ensemble import GradientBoostingRegressor
import matplotlib.pyplot as plt
####################

def infographics(df, df99, df999):

    #code to generate density heatmap of attacks all over the world; group csv by country_txt create column 'freq' that 
    #contains count of the number of times a country has been mentioned; create new df that contains country_txt, freq
    #lat and long and drops the duplicates in the original df; convert the number of attacks per country to a list;
    #create a variable that defines the size of the radius of the marker used in the map; plot map and store in fig
    df['freq'] = df.groupby('country_txt')['country'].transform('count')
    df8 = df.filter(['country_txt','freq','latitude','longitude']).drop_duplicates()
    df8=df8.rename(columns={'country_txt' : 'Country', 'freq' : 'Crimes','latitude' : 'Latitude','longitude' : 'Longitude'})
    list1 = df8.Crimes.values.tolist()
    m = interp1d([1,max(list1)],[5,20])
    circle = m(list1)
    fig = px.density_mapbox(df8, lat='Latitude', lon='Longitude', radius=circle, zoom=1, mapbox_style='open-street-map', hover_data=["Country", "Crimes"])
    fig.update_layout(
        title={"text" : "Density Map", 'x' : 0.5, 'y' : 0.95},
        font=dict(size=18)
        )

    # url = "D:\Python\Programs\Demo Folder\CrimeAnalytics\GTD_Analysis-master\Predicted_location.csv"
    df1 = pd.read_csv("D:\Python\Programs\Demo Folder\CrimeAnalytics\GTD_Analysis-master\pred_loc.csv")
    # print(df1)
    lat=df1['Latitude']
    lon=df1['Longitude']
    add=df1['address']
    fig111 = px.density_mapbox(df1, lat='Latitude', lon='Longitude', hover_data=['address'],radius=10, zoom=4, mapbox_style='open-street-map'
                            )
    fig111.update_layout(
        title={"text" : "Density Map", 'x' : 0.5, 'y' : 0.95},
        font=dict(size=18)
        )


    #Code that creates stacked bar chart displaying the number of attacks per year for each attack type
    #Filter year and attacktype and find the number of attacks for each attacktype per year then plot the graph
    df60 = df.filter(["iyear", "attacktype1_txt"], axis=1)
    count_series = df60.groupby(["iyear", "attacktype1_txt"]).size()
    df61 = count_series.to_frame(name = 'size').reset_index()
    df62 = df61.pivot(index='iyear', columns='attacktype1_txt', values='size')
    fig1 = df62.iplot(kind='bar', barmode='stack', asFigure=True)
    fig1.update_layout(
        title={"text" : "Comparison of Crime Types using Stacked Bar Chart", 'x' : 0.5, 'y' : 0.95},
        font=dict(size=18),
        legend = dict(font = dict(size=14))
        )

    #Bar graph with a slider for year that shows number of people killed per region for every year
    df70 = df.filter(["region_txt", "nkill", "iyear"], axis=1)
    df70 = df70[df70['nkill']!=-1]
    count_series = df70.groupby(['iyear', 'region_txt'])['nkill'].sum()
    df71 = count_series.to_frame(name = 'kills').reset_index()
    fig10 = px.bar(df71, y="region_txt", x="kills", animation_frame="iyear", 
                    color="region_txt", hover_name="region_txt", hover_data=['iyear', 'kills'],
                labels={'iyear' : 'Year', 'region_txt' : 'Region', 'kills' : 'Fatalities'},
                width=1100, orientation='h')
    fig["layout"].pop("updatemenus")
    fig10.update_layout(
        title={"text" : "People killed according to region", 'x' : 0.5, 'y' : 0.95},
        font=dict(size=18),
        legend = dict(font = dict(size=14))
        )


    #Heatmap which shows a coloured representation of the total number of attacks per region
    #count the number of times each region has been mentioned in the csv and store that as the freq of attacks for 
    #that region. Reshape them to numpy arrays and create a heatmap
    df['freq'] = df.groupby('region_txt')['region_txt'].transform('count')
    dfr = df.filter(['region_txt', 'freq']).drop_duplicates()
    r=dfr['region_txt'].values.reshape(4, 3)
    fr=dfr['freq'].values.reshape(4, 3)
    fig3 = ff.create_annotated_heatmap(fr, annotation_text=r, colorscale='Viridis', hoverinfo='z', showscale=True)
    fig3.update_layout(
        title={"text" : "Region-Wise Concentration of Crimes using Heatmap", 'x' : 0.5, 'y' : 0.93},
        font=dict(size=18),
        )


    #Scattered GeoPlot that shows the total attacks per region per year along with date, city and location
    fig4 = px.scatter_geo(df, lat='latitude', lon='longitude',
                        hover_name="country_txt", hover_data=['iyear', 'city'],
                        labels={'iyear':'Year', 'city': 'City', 'latitude':'Latitude', 'longitude':'Longitude'},
                        animation_frame="iyear", color = "region_txt",
                        projection="natural earth")
    fig4.update_layout(
        title={"text" : "Global Crimes History using ScatterGeo Plot", 'x' : 0.5, 'y' : 0.98},
        font=dict(size=18),
        )



    #create df for india infographics 
    df200 = df.loc[df['country_txt'] == 'United States']
    # df200['provstate'] = df200['provstate'].replace(['Andhra Pradesh'],'Andhra Pradesh')


    #Statewise attack types and frequency for all years
    df80 = df200.filter(["provstate", "attacktype1_txt"], axis=1)
    count_series = df80.groupby(["provstate", "attacktype1_txt"]).size()
    df81 = count_series.to_frame(name = 'size').reset_index()
    df82 = df81.pivot(index='provstate', columns='attacktype1_txt', values='size')
    fig82 = df82.iplot(kind='bar', barmode='stack', asFigure=True)
    fig82.update_layout(
        title={"text" : "Comparison of Crime Types using Stacked Bar Chart- statewise", 'x' : 0.48, 'y' : 0.95},
        font=dict(size=14),
        legend = dict(font = dict(size=15)),
        xaxis_tickangle=-45
        )


    #People killed in each state per year
    df30 = df200.filter(["provstate", "nkill", "iyear"], axis=1)
    df30 = df30[df30['nkill']!=-1]
    count_series = df30.groupby(['iyear','provstate'])['nkill'].sum()
    df31 = count_series.to_frame(name = 'kills').reset_index()
    fig31 = px.bar(df31, x='provstate', y='kills', animation_frame="iyear", animation_group="provstate",
                hover_data=['provstate', 'kills'], color='kills',
                labels={'provstate': 'States', 'kills': 'Total Fatalities'}, height=600, width=1000)
    fig31.update_layout(xaxis_tickangle=-45, 
                        width=1370,
                        height=700,
                        margin=dict(pad=20),
                    title={"text" : "People killed according to state per year", 'x' : 0.5, 'y' : 0.98})
    fig31.update_xaxes(automargin=True)

    #Total fatalities according to each state
    df32 = df200.filter(["provstate", "nkill", "iyear"], axis=1)
    df32 = df32[df32['nkill']!=-1]
    count_series = df32.groupby(['provstate'])['nkill'].sum()
    df33 = count_series.to_frame(name = 'kills').reset_index()
    fig32 = px.bar(df33, x='provstate', y='kills',
                hover_data=['provstate', 'kills'], color='kills',
                labels={'provstate': 'States', 'kills': 'Total Fatalities'}, height=600, width=1000)
    fig32.update_layout(xaxis_tickangle=-45, 
                        width=1350,
                        height=700,
                    title={"text" : "People killed according to state overall", 'x' : 0.5, 'y' : 0.97})
    fig32.update_xaxes(automargin=True)


    #pie chart of weapon types used in all attacks 
    df42 = df200.filter(["weaptype1_txt", "iyear"], axis=1)
    df42.head()
    count_series = df42.groupby(['weaptype1_txt'])['iyear'].count()
    df43 = count_series.to_frame(name = 'weap').reset_index()
    fig42 = px.pie(df43, values='weap', names='weaptype1_txt', labels={'weap':'No. of times weapon used', 'weaptype1_txt': 'Weapon_Type'},
                title='Types of weapons used in attacks in United States', hole=.3)
    fig42.update_traces(textposition='inside', textinfo='percent+label')


    #Density Heatmap with year by year animation that displays additional info
    df2 = df200
    df2['freq'] = df2.groupby(['provstate', 'iyear'])['provstate'].transform('count')
    df2['freq'] = df2['freq'].fillna(0).astype(int)

    df2 = df2.filter(['provstate','freq','latitude','longitude', 'iyear','city','attacktype1_txt','imonth',
                    'iday','nkill','attacktype1']).drop_duplicates()
    df2=df2.rename(columns={'provstate' : 'State', 'freq' : 'Crimes_that_year_in_state','latitude' : 'Latitude', 'longitude' : 'Longitude',
                            'iyear': 'Year', 'city': 'City', 'attacktype1_txt': 'Crime_Type', 'imonth': 'Month', 'iday': 'Date',
                            'nkill': 'Fatalities', 'attacktype1': 'Crime_Type_'})
    list2 = df2.Crimes_that_year_in_state.values.tolist()
    # list2=list2.notnull()
    m = interp1d([0,max(list2)],[5,20])
    circle = m(list2)
    fig40 = px.density_mapbox(df2, lat='Latitude', lon='Longitude', radius=circle, zoom=1, mapbox_style='open-street-map', 
                            hover_data=["State", "City", "Crimes_that_year_in_state", "Fatalities", "Crime_Type" ],
                            animation_frame='Year')
    fig40.update_layout(
        margin={"r":0,"t":32,"l":1,"b":0},
        title='Density map with year-wise attack information',
        mapbox=go.layout.Mapbox(
            style="open-street-map", 
            zoom=3, 
             center_lat = 29.813822,
             center_lon = -95.365295,
        )
    )


    #World explorer plot
    filter_options = ['Property Damage', 'Target Nationality', 'Target Type', 'Type of Crime', 'Weapon Type', 'Region', 'Country']
    df99['Attacks'] = df99.groupby(['country_txt', 'iyear'])['country_txt'].transform('count')
    dfr = df99.filter(['country_txt', 'Attacks', 'iyear']).drop_duplicates()
    fig50 = px.area(dfr, x="iyear", y="Attacks", color="country_txt", line_group="country_txt")


    #India explorer plot
    filter_india_options = ['Terrorist Organization', 'Property Damage', 'Target Type', 'Type of Crimes', 'Weapon Type', 'State']
    df999=df999[df.country_txt.eq('United States')]
    df999['Attacks'] = df999.groupby(['provstate', 'iyear'])['provstate'].transform('count')
    dfr2 = df999.filter(['provstate', 'Attacks', 'iyear']).drop_duplicates()
    dfr2['Attacks'] = dfr2['Attacks'].fillna(1).astype(int)
    # fig51 = px.area(dfr2, x="iyear", y="Attacks", color="provstate", line_group="provstate")
    fig51 = px.area(dfr2, x="iyear", y="Attacks")

    ####################################################################################################33


    return fig, fig1, fig10, fig3, fig4, fig82, fig31, fig32, fig42, fig40, fig50, fig51,fig111, filter_options, filter_india_options